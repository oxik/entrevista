JavaScript
==========

* Modifica la porción de código indicada en ejercicio-1.js de modo que la salida sea "la esperada" (1 2 3 4 5 6 7 8 9 10)
* Modificar el prototípo de los objetos String de JavaScript en el fichero ejercicio-2.js, de modo que nuestros strings puedan hacer uso del método espaciar. El resultado esperado es "E s t o  e s  u n a  p r u e b a"