var $ = require('jquery-browserify');

$(function() {
	var template = require('./template.hbs');
	var data = {
		mensaje : 'parece que <strong>sabemos lo que hacemos</strong>...'
	};

	$('#container').html(template(data));
});