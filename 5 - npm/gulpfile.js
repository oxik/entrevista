var gulp        = require('gulp');
var uglify      = require('gulp-uglify');
var browserify  = require('gulp-browserify');

gulp.task('scripts', function() {
	return gulp.src('assets/ejercicio.js')
		.pipe(browserify({ transform: ['hbsfy'] }))
		.pipe(uglify())
		.pipe(gulp.dest('src'));
});

gulp.task('default', function() {
	gulp.watch(['assets/ejercicio.js'],  ['scripts']);
});